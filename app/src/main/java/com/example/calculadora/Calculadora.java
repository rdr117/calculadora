package com.example.calculadora;

public class Calculadora {
    private float num1;
    private float num2;

    public Calculadora(float num1, float num2) {
        // constructor por argumentos
        this.setNum1(num1);
        this.setNum2(num2);
    }
    // Metodos para cambiar y obtener el estado del objeto

    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    //metodos de comportamiento

    public float sumar(){return this.num1 + this.num2;}
    public float restar(){return this.num1 - this.num2;}
    public float multiplicar(){return this.num1 * this.num2;}
    public float division(){return this.num1 / this.num2;}

}