package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNum1;
    private EditText txtNum2;
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private TextView lblResultado;
    private Calculadora calculadora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);

        btnSumar = (Button) findViewById(R.id.btnSumar);
        btnRestar = (Button)findViewById(R.id.btnRestar);
        btnMultiplicar = (Button) findViewById(R.id.btnMultiplicar);
        btnDividir = (Button) findViewById(R.id.btnDividir);

        lblResultado = (TextView) findViewById(R.id.lblresultado);

        calculadora = new Calculadora(0.0f,0.0f);

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar
                if(txtNum1.getText().toString().matches("")||
                txtNum2.getText().toString().matches("")
                ){

                    Toast.makeText(MainActivity.this,"Falto capturar numero",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                float num1 = Float.parseFloat( txtNum1.getText().toString());
                float num2 = Float.parseFloat( txtNum1.getText().toString());
                calculadora.setNum1(num1);
                calculadora.setNum2(num2);
                float res = calculadora.sumar();
                lblResultado.setText(String.valueOf(res));}
            }

        });
        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar
                if(txtNum1.getText().toString().matches("")||
                        txtNum2.getText().toString().matches("")
                ){
                    Toast.makeText(MainActivity.this,"Falto capturar numero",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                float num1 = Float.parseFloat( txtNum1.getText().toString());
                float num2 = Float.parseFloat( txtNum1.getText().toString());
                calculadora.setNum1(num1);
                calculadora.setNum2(num2);
                float res = calculadora.restar();
                lblResultado.setText(String.valueOf(res));}

    }
        });
        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar
                if(txtNum1.getText().toString().matches("")||
                        txtNum2.getText().toString().matches("")
                ){
                    Toast.makeText(MainActivity.this,"Falto capturar numero",
                            Toast.LENGTH_SHORT).show();
                }
                else{
                float num1 = Float.parseFloat( txtNum1.getText().toString());
                float num2 = Float.parseFloat( txtNum1.getText().toString());
                calculadora.setNum1(num1);
                calculadora.setNum2(num2);
                float res = calculadora.multiplicar();
                lblResultado.setText(String.valueOf(res));}

            }
        });
        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Validar
                if(txtNum1.getText().toString().matches("")||
                txtNum2.getText().toString().matches("")
                ){
                }
                else   {
                float num1 = Float.parseFloat( txtNum1.getText().toString());
                float num2 = Float.parseFloat( txtNum1.getText().toString());
                calculadora.setNum1(num1);
                calculadora.setNum2(num2);
                float res = calculadora.division();
                lblResultado.setText(String.valueOf(res));}

            }
        });

}}
